﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HomeRent.Models;

namespace HomeRent.Controllers
{
    public class ForRentController : Controller
    {
        private HomeRentDB db = new HomeRentDB();

        // GET: /ForRent/
        public ActionResult Index()
        {
            var forrents = db.ForRents.Include(f => f.Flat);
            return View(forrents.ToList());
        }

        // GET: /ForRent/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ForRent forrent = db.ForRents.Find(id);
            if (forrent == null)
            {
                return HttpNotFound();
            }
            return View(forrent);
        }

        // GET: /ForRent/Create
        public ActionResult Create()
        {
            ViewBag.FlatID = new SelectList(db.Flats, "ID", "Name");
            return View();
        }

        // POST: /ForRent/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ID,Date,AvailableDate,FlatID")] ForRent forrent)
        {
            if (ModelState.IsValid)
            {
                db.ForRents.Add(forrent);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.FlatID = new SelectList(db.Flats, "ID", "Name", forrent.FlatID);
            return View(forrent);
        }

        // GET: /ForRent/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ForRent forrent = db.ForRents.Find(id);
            if (forrent == null)
            {
                return HttpNotFound();
            }
            ViewBag.FlatID = new SelectList(db.Flats, "ID", "Name", forrent.FlatID);
            return View(forrent);
        }

        // POST: /ForRent/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ID,Date,AvailableDate,FlatID")] ForRent forrent)
        {
            if (ModelState.IsValid)
            {
                db.Entry(forrent).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.FlatID = new SelectList(db.Flats, "ID", "Name", forrent.FlatID);
            return View(forrent);
        }

        // GET: /ForRent/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ForRent forrent = db.ForRents.Find(id);
            if (forrent == null)
            {
                return HttpNotFound();
            }
            return View(forrent);
        }

        // POST: /ForRent/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ForRent forrent = db.ForRents.Find(id);
            db.ForRents.Remove(forrent);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
