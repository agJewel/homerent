﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HomeRent.Models;

namespace HomeRent.Controllers
{
    public class FlatController : Controller
    {
        private HomeRentDB db = new HomeRentDB();

        // GET: /Flat/
        public ActionResult Index()
        {
            var uid = Convert.ToInt32(Session["ID"]);
            var flats = db.Flats.Include(f => f.House).Where(flt=>flt.House.UserID == uid);
            return View(flats.ToList());
        }
        public string GetFlatSize(int id=0)
        {
            return db.Flats.Find(id).Size.ToString();
        }
        

        public ActionResult List()
        {
            var flats = db.Flats.Include(f => f.House);
            return View(flats.ToList());
        }

        // GET: /Flat/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Flat flat = db.Flats.Find(id);
            if (flat == null)
            {
                return HttpNotFound();
            }
            return View(flat);
        }

        // GET: /Flat/Create
        public ActionResult Create()
        {
            var uid = Convert.ToInt32(Session["ID"]);
            ViewBag.CategoryID = new SelectList(db.Categories, "ID", "Name");
            ViewBag.HouseID = new SelectList(db.Houses.Where(hs=>hs.UserID == uid), "ID", "Name");
            return View();
        }

        // POST: /Flat/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public ActionResult Create([Bind(Include="ID,Name,Size,HouseID,CategoryID")] Flat flat)
        {
            
            if (ModelState.IsValid)
            {
                //Session["ID"] = HouseID;
                db.Flats.Add(flat);
                db.SaveChanges();
                //return RedirectToAction("Create","HouseRent");
                return RedirectToAction("Index");
            }
            ViewBag.CategoryID = new SelectList(db.Categories, "ID", "Name", flat.CategoryID);
            ViewBag.HouseID = new SelectList(db.Houses.Where(h=>h.UserID==flat.HouseID), "ID", "Name");
            
            return View(flat);
        }

        // GET: /Flat/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Flat flat = db.Flats.Find(id);
            if (flat == null)
            {
                return HttpNotFound();
            }
            ViewBag.HouseID = new SelectList(db.Houses, "ID", "Name", flat.HouseID);
            return View(flat);
        }

        // POST: /Flat/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ID,Name,Size,HouseID")] Flat flat)
        {
            if (ModelState.IsValid)
            {
                db.Entry(flat).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.HouseID = new SelectList(db.Houses, "ID", "Name", flat.HouseID);
            return View(flat);
        }

        // GET: /Flat/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Flat flat = db.Flats.Find(id);
            if (flat == null)
            {
                return HttpNotFound();
            }
            return View(flat);
        }

        // POST: /Flat/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Flat flat = db.Flats.Find(id);
            db.Flats.Remove(flat);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
