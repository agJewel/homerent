﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HomeRent.Models;

namespace HomeRent.Controllers
{
    
    public class HouseRentController : Controller
    {
        private HomeRentDB db = new HomeRentDB();
        User user=new User();
      
        // GET: /HouseRent/
        public ActionResult Index()
        {
            var uid = Convert.ToInt32(Session["ID"]);

            var houserents = db.HouseRents.Where(h=>h.Flat.House.UserID==uid).Include(h => h.Area).Include(h => h.Category).Include(h => h.Flat);
            return View(houserents.ToList());
        }

        public JsonResult GetCategory(int id = 0)
        {
            return Json(new SelectList(db.Categories.Where(c => c.ID == id), "ID", "Name"));
        }


        public string GetRent(int catid = 0, int arid = 0)
        {
            return db.FixRents.Where(fr => fr.CategoryID == catid && fr.AreaID == arid).FirstOrDefault().Rent.ToString();
        }

       
        // GET: /HouseRent/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HouseRent houserent = db.HouseRents.Find(id);
            if (houserent == null)
            {
                return HttpNotFound();
            }
            return View(houserent);
        }

        // GET: /HouseRent/Create
        public ActionResult Create()
        {
            var uid = Convert.ToInt32(Session["ID"]);
            
            ViewBag.AreaID = new SelectList(db.Areas, "ID", "Name");
            ViewBag.CategoryID = new SelectList(db.Categories, "ID", "Name");
            ViewBag.FlatID = new SelectList(db.Flats.Where(f => f.House.UserID == uid), "ID", "Name");
            ViewBag.HouseID = new SelectList(db.Houses.Where(h => h.UserID == uid), "ID", "Name");
      
            return View();
        }

        // POST: /HouseRent/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ID,Rent_PSFT,FlatID,AreaID,CategoryID,FlatSize,TotalRent")] HouseRent houserent,Area area)
        {
            if (ModelState.IsValid)
            {
                db.HouseRents.Add(houserent);
                db.SaveChanges();
                
                return RedirectToAction("Index");
            }
          
            ViewBag.AreaID = new SelectList(db.Areas, "ID", "Name", houserent.AreaID);
            ViewBag.CategoryID = new SelectList(db.Categories, "ID", "Name", houserent.CategoryID);
            ViewBag.FlatID = new SelectList(db.Flats, "ID", "Name", houserent.FlatID);
            //ViewBag.HouseID = new SelectList(db.Houses, "ID", "Name", houserent.HouseID);
            return View(houserent);
        }

        // GET: /HouseRent/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HouseRent houserent = db.HouseRents.Find(id);
            if (houserent == null)
            {
                return HttpNotFound();
            }
            ViewBag.AreaID = new SelectList(db.Areas, "ID", "Name", houserent.AreaID);
            ViewBag.CategoryID = new SelectList(db.Categories, "ID", "Name", houserent.CategoryID);
            ViewBag.FlatID = new SelectList(db.Flats, "ID", "Name", houserent.FlatID);
            return View(houserent);
        }

        // POST: /HouseRent/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ID,MinRent_PSFT,MaxRent_PSFT,FlatID,AreaID,CategoryID")] HouseRent houserent)
        {
            if (ModelState.IsValid)
            {
                db.Entry(houserent).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AreaID = new SelectList(db.Areas, "ID", "Name", houserent.AreaID);
            ViewBag.CategoryID = new SelectList(db.Categories, "ID", "Name", houserent.CategoryID);
            ViewBag.FlatID = new SelectList(db.Flats, "ID", "Name", houserent.FlatID);
            return View(houserent);
        }

        // GET: /HouseRent/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HouseRent houserent = db.HouseRents.Find(id);
            if (houserent == null)
            {
                return HttpNotFound();
            }
            return View(houserent);
        }

        // POST: /HouseRent/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            HouseRent houserent = db.HouseRents.Find(id);
            db.HouseRents.Remove(houserent);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
