﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HomeRent.Models;

namespace HomeRent.Controllers
{
    public class RoadController : Controller
    {
        private HomeRentDB db = new HomeRentDB();

        // GET: /Road/
        public ActionResult Index()
        {
            var roads = db.Roads.Include(r => r.Area);
            return View(roads.ToList());
        }

        // GET: /Road/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Road road = db.Roads.Find(id);
            if (road == null)
            {
                return HttpNotFound();
            }
            return View(road);
        }

        // GET: /Road/Create
        public ActionResult Create()
        {
            ViewBag.AreaID = new SelectList(db.Areas, "ID", "Name");
            return View();
        }

        // POST: /Road/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ID,Name,AreaID")] Road road)
        {
            if (ModelState.IsValid)
            {
                db.Roads.Add(road);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AreaID = new SelectList(db.Areas, "ID", "Name", road.AreaID);
            return View(road);
        }

        // GET: /Road/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Road road = db.Roads.Find(id);
            if (road == null)
            {
                return HttpNotFound();
            }
            ViewBag.AreaID = new SelectList(db.Areas, "ID", "Name", road.AreaID);
            return View(road);
        }

        // POST: /Road/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ID,Name,AreaID")] Road road)
        {
            if (ModelState.IsValid)
            {
                db.Entry(road).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AreaID = new SelectList(db.Areas, "ID", "Name", road.AreaID);
            return View(road);
        }

        // GET: /Road/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Road road = db.Roads.Find(id);
            if (road == null)
            {
                return HttpNotFound();
            }
            return View(road);
        }

        // POST: /Road/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Road road = db.Roads.Find(id);
            db.Roads.Remove(road);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
