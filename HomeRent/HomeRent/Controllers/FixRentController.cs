﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HomeRent.Models;

namespace HomeRent.Controllers
{
    public class FixRentController : Controller
    {
        private HomeRentDB db = new HomeRentDB();

        // GET: /FixRent/
        public ActionResult Index()
        {
            var fixrents = db.FixRents.Include(f => f.Area);
            return View(fixrents.ToList());
        }

        //public JsonResult GetCategory(int id = 0)
        //{
        //    return Json(new SelectList(db.Categories.Where(c => c.ID == id), "ID", "Name"));
        //}


        //public string GetRent(int catid = 0, int arid = 0)
        //{
        //    return db.FixRents.Where(fr => fr.CategoryID == catid && fr.AreaID == arid).FirstOrDefault().Rent.ToString();
        //}
       
       

        // GET: /FixRent/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FixRent fixrent = db.FixRents.Find(id);
            if (fixrent == null)
            {
                return HttpNotFound();
            }
            return View(fixrent);
        }

        // GET: /FixRent/Create
        public ActionResult Create()

        {
            ViewBag.CategoryID = new SelectList(db.Categories, "ID", "Name");
            ViewBag.AreaID = new SelectList(db.Areas, "ID", "Name");
            return View();
        }

        // POST: /FixRent/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ID,Rent,AreaID,CategoryID")] FixRent fixrent,int CategoryID=0)
        {
            if (ModelState.IsValid)
            {
                db.FixRents.Add(fixrent);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CategoryID = new SelectList(db.Categories, "ID", "Name", fixrent.CategoryID);
            ViewBag.AreaID = new SelectList(db.Areas, "ID", "Name", fixrent.AreaID);
            return View(fixrent);
        }

        // GET: /FixRent/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FixRent fixrent = db.FixRents.Find(id);
            if (fixrent == null)
            {
                return HttpNotFound();
            }
            ViewBag.AreaID = new SelectList(db.Areas, "ID", "Name", fixrent.AreaID);
            return View(fixrent);
        }

        // POST: /FixRent/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ID,Rent,AreaID,CategoryID")] FixRent fixrent)
        {
            if (ModelState.IsValid)
            {
                db.Entry(fixrent).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AreaID = new SelectList(db.Areas, "ID", "Name", fixrent.AreaID);
            return View(fixrent);
        }

        // GET: /FixRent/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FixRent fixrent = db.FixRents.Find(id);
            if (fixrent == null)
            {
                return HttpNotFound();
            }
            return View(fixrent);
        }

        // POST: /FixRent/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            FixRent fixrent = db.FixRents.Find(id);
            db.FixRents.Remove(fixrent);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
