﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HomeRent.Models;

namespace HomeRent.Controllers
{
    public class HouseController : Controller
    {
        private HomeRentDB db = new HomeRentDB();

        // GET: /House/
        public ActionResult Index()
        {
            var uid = Convert.ToInt32(Session["ID"]);
            var houses = db.Houses.Where(a=>a.UserID==uid).Include(h => h.Road);
            return View(houses.ToList());
        }
        public ActionResult list()
        {
            var houses = db.Houses.Include(h => h.Road);
            return View(houses.ToList());
        }

        public ActionResult MyFlats(int houseId=0)
        {
            Session["houseId"] = Convert.ToInt32(houseId);
            List<Flat> myFlats = new List<Flat>();
            var flats = db.Flats.Where(f => f.HouseID == houseId).ToList();

            foreach (var f in flats)
            {
                myFlats.Add(f);
            }
            ViewBag.myHouseId = houseId;
            return View(myFlats);
        }
        public List<House> MyHouses(int id)
        {
            List<House> myHouses=new List<House>();
            var house=db.Houses.Where(h=>h.UserID==id).ToList();

            foreach (var h in house)
            {
                myHouses.Add(h);
            }
            return myHouses;
        }
        public JsonResult GetArea(int cityId = 0)
        {
            return Json(new SelectList(db.Areas.Where(a=>a.CityID==cityId),"ID","Name"));
        }
        public JsonResult GetRoad(int areaId = 0)
        {
            return Json(new SelectList(db.Roads.Where(ro=> ro.AreaID == areaId), "ID", "Name"));
        }
        // GET: /House/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            House house = db.Houses.Find(id);
            if (house == null)
            {
                return HttpNotFound();
            }
            return View(house);
        }

        // GET: /House/Create
        public ActionResult Create()
        {
            ViewBag.City = new SelectList(db.Cities, "ID", "Name");
            ViewBag.Area=new SelectList(new List<Area>(),"ID","Name");
            ViewBag.RoadID = new SelectList(new List<Road>(), "ID", "Name");
            return View();
        }

        // POST: /House/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
    
        public ActionResult Create([Bind(Include="ID,Name,OwnerName,NID,PhoneNumber,DateCreated,HouseNo,RoadID")] House house,int cityId=0,int areaId=0,int roadId=0 )
        {
       
            if (ModelState.IsValid)
            {
                house.UserID = Convert.ToInt32(Session["ID"]);
                
                db.Houses.Add(house);
                db.SaveChanges();
                return RedirectToAction("Create","Flat"); 
            }
            ViewBag.Username = Convert.ToInt32(Session["ID"]);

          
            ViewBag.City = new SelectList(db.Cities, "ID", "Name",cityId);
            ViewBag.Area = new SelectList(db.Areas.Where(ar => ar.CityID==cityId), "ID", "Name", areaId);
            ViewBag.RoadID = new SelectList(db.Roads.Where(ro=>ro.AreaID==areaId),"ID","Name",roadId);
            return View(house);
        }


        // GET: /House/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            House house = db.Houses.Find(id);
            if (house == null)
            {
                return HttpNotFound();
            }
            ViewBag.RoadID = new SelectList(db.Roads, "ID", "Name", house.RoadID);
            return View(house);
        }

        // POST: /House/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ID,Name,OwnerName,NID,PhoneNumber,DateCreated,RoadID")] House house)
        {
            if (ModelState.IsValid)
            {
                db.Entry(house).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.RoadID = new SelectList(db.Roads, "ID", "Name", house.RoadID);
            return View(house);
        }

        // GET: /House/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            House house = db.Houses.Find(id);
            if (house == null)
            {
                return HttpNotFound();
            }
            return View(house);
        }

        // POST: /House/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            House house = db.Houses.Find(id);
            db.Houses.Remove(house);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
        //public JsonResult GetCategory(int id = 0)
        //{
        //    return Json(new SelectList(db.Categories.Where(c=>c.ID==id),"ID","Name"));
        //}
        

        //public string GetRent(int id = 0)
        //{
        //    return db.FixRents.Find(id).Rent;
        //}