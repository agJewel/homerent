﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HomeRent.Models;


namespace HomeRent.Controllers
{
    public class RentSlipController : Controller
    {
        private HomeRentDB db = new HomeRentDB();

         //GET: RentSlip
        public ActionResult Index()
        {
            var uid = Convert.ToInt32(Session["ID"]);
            var rentslips = db.RentSlips.Where(h=>h.Flat.House.UserID==uid).Include(r => r.Flat).Include(r => r.House);
            return View(rentslips.ToList());

        }

        // GET: /RentSlip/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RentSlip rentslip = db.RentSlips.Find(id);
            if (rentslip == null)
            {
                return HttpNotFound();
            }
            return View(rentslip);
        }
        public JsonResult GetFlat(int id = 0)
        {
            return Json(new SelectList(db.Flats.Where(c => c.HouseID == id), "ID", "Name"));
        }

        public string GetTotalRent(int id = 0)
        {
            return db.HouseRents.FirstOrDefault(d => d.FlatID == id).TotalRent.ToString();
        }

       

        // GET: /RentSlip/Create
        public ActionResult Create()
        {
            var uid = Convert.ToInt32(Session["ID"]);
            ViewBag.FlatID = new SelectList(db.Flats.Where(f=>f.House.UserID==uid), "ID", "Name");
            ViewBag.HouseID = new SelectList(db.Houses.Where(h => h.UserID==uid), "ID", "Name");
            return View();
        }

        // POST: /RentSlip/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ID,Date,RentSlipNo,OwnerName,Bank,AccountNumber,OwnerNID,RenterName,RenterNID,RenterProfession,Rent,Tax,HouseID,FlatID")] RentSlip rentslip,string ONid,string RNid)
        {
            if (ModelState.IsValid)
            {
              
                    db.RentSlips.Add(rentslip);
                    db.SaveChanges();
                    return RedirectToAction("index");
                }
          

            ViewBag.FlatID = new SelectList(db.Flats, "ID", "Name", rentslip.FlatID);
            ViewBag.HouseID = new SelectList(db.Houses, "ID", "Name", rentslip.HouseID);
            return View(rentslip);
        }

        // GET: /RentSlip/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RentSlip rentslip = db.RentSlips.Find(id);
            if (rentslip == null)
            {
                return HttpNotFound();
            }
            ViewBag.FlatID = new SelectList(db.Flats, "ID", "Name", rentslip.FlatID);
            ViewBag.HouseID = new SelectList(db.Houses, "ID", "Name", rentslip.HouseID);
            return View(rentslip);
        }

        // POST: /RentSlip/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ID,Date,RentSlipNo,OwnerName,Bank,AccountNumber,OwnerNID,RenterName,RenterNID,RenterProfession,Rent,Tax,HouseID,FlatID")] RentSlip rentslip)
        {
            if (ModelState.IsValid)
            {
                db.Entry(rentslip).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.FlatID = new SelectList(db.Flats, "ID", "Name", rentslip.FlatID);
            ViewBag.HouseID = new SelectList(db.Houses, "ID", "Name", rentslip.HouseID);
            return View(rentslip);
        }

        // GET: /RentSlip/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RentSlip rentslip = db.RentSlips.Find(id);
            if (rentslip == null)
            {
                return HttpNotFound();
            }
            return View(rentslip);
        }

        // POST: /RentSlip/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            RentSlip rentslip = db.RentSlips.Find(id);
            db.RentSlips.Remove(rentslip);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
