﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HomeRent.Models;

namespace HomeRent.Controllers
{
    public class UserController : Controller
    {
        private HomeRentDB db = new HomeRentDB();

        // GET: /User/
        public ActionResult Index()
        {
            return View(db.Users.ToList());
        }


        public ActionResult Login()
        {
            return View();
        }

        public ActionResult LogOut()
        {
            if (ModelState.IsValid)
            {
                Session.Clear();
            }
            return View();
        }
        [HttpPost]
   
        public ActionResult Login(Login login)
        {
            if (ModelState.IsValid)
            {
                var v =
                    db.Users.Where(usr => usr.Email.ToLower() == login.Email.ToLower() & usr.Password == login.Password)
                        .ToList();
              
                if (v.Count > 0)
                {
                    Session["ID"] = v.First().ID;
                    
                    Session["Name"] = v.First().Name;
                    Session["Email"] = v.First().Email;
                    Session["Password"] = v.First().Password;
                    Session["Role"] = v.First().Role;
                    return RedirectToAction("Create","House");

                }
                else
                {
                   // ViewBag.LoginError = "Invalid User name or password";
                    Session["error"] = "Invalid User name or password";
                }
            }
            return View();
        }

        public ActionResult LoginSuccessful()
        {
            return View();
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register([Bind(Include = "ID,Name,NID,PhoneNumber,Profession,Email,Password,Role")]User user)
        {
            if (ModelState.IsValid)
            {
                db.Users.Add(user);
                db.SaveChanges();
                ViewBag.Message = "Registration Successfull";
                return RedirectToAction("Login", "User");
               
            }
              ModelState.Clear();
            

            return View();
        }


        // GET: /User/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // GET: /User/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /User/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ID,Name,NID,PhoneNumber,Profession,Email,Password,Role")] User user)
        {
            if (ModelState.IsValid)
            {
                user.Name = Request["txtName"];
                user.NID = Request["txtNid"];
                user.PhoneNumber = Request["txtPhone"];
                user.Profession = Request["txtProfession"];
                user.Email = Request["txtEmail"];
                user.Password = Request["txtPassword"];
                user.Role = (Request["txtRole"]);

                db.Users.Add(user);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(user);
        }

        // GET: /User/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: /User/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ID,Name,NID,PhoneNumber,Profession")] User user)
        {
            if (ModelState.IsValid)
            {
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(user);
        }

        // GET: /User/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: /User/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            User user = db.Users.Find(id);
            db.Users.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
