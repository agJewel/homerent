﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HomeRent.Models;

namespace HomeRent.Controllers
{
    public class RentedController : Controller
    {
        private HomeRentDB db = new HomeRentDB();

        // GET: /Rented/
        public ActionResult Index()
        {
            var renteds = db.Renteds.Include(r => r.Flat).Include(r => r.User);
            return View(renteds.ToList());
        }

        // GET: /Rented/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Rented rented = db.Renteds.Find(id);
            if (rented == null)
            {
                return HttpNotFound();
            }
            return View(rented);
        }

        // GET: /Rented/Create
        public ActionResult Create()
        {
            ViewBag.FlatID = new SelectList(db.Flats, "ID", "Name");
            ViewBag.UserID = new SelectList(db.Users, "ID", "Name");
            return View();
        }

        // POST: /Rented/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ID,Date,Rate,Remarks,Tax_,FlatID,UserID")] Rented rented)
        {
            if (ModelState.IsValid)
            {
                db.Renteds.Add(rented);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.FlatID = new SelectList(db.Flats, "ID", "Name", rented.FlatID);
            ViewBag.UserID = new SelectList(db.Users, "ID", "Name", rented.UserID);
            return View(rented);
        }

        // GET: /Rented/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Rented rented = db.Renteds.Find(id);
            if (rented == null)
            {
                return HttpNotFound();
            }
            ViewBag.FlatID = new SelectList(db.Flats, "ID", "Name", rented.FlatID);
            ViewBag.UserID = new SelectList(db.Users, "ID", "Name", rented.UserID);
            return View(rented);
        }

        // POST: /Rented/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ID,Date,Rate,Remarks,Tax_,FlatID,UserID")] Rented rented)
        {
            if (ModelState.IsValid)
            {
                db.Entry(rented).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.FlatID = new SelectList(db.Flats, "ID", "Name", rented.FlatID);
            ViewBag.UserID = new SelectList(db.Users, "ID", "Name", rented.UserID);
            return View(rented);
        }

        // GET: /Rented/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Rented rented = db.Renteds.Find(id);
            if (rented == null)
            {
                return HttpNotFound();
            }
            return View(rented);
        }

        // POST: /Rented/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Rented rented = db.Renteds.Find(id);
            db.Renteds.Remove(rented);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
