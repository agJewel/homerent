USE [master]
GO
/****** Object:  Database [HomeRentDB]    Script Date: 6/23/2016 1:17:38 AM ******/
CREATE DATABASE [HomeRentDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'HomeRentDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\HomeRentDB.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'HomeRentDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\HomeRentDB_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [HomeRentDB] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [HomeRentDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [HomeRentDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [HomeRentDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [HomeRentDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [HomeRentDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [HomeRentDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [HomeRentDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [HomeRentDB] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [HomeRentDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [HomeRentDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [HomeRentDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [HomeRentDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [HomeRentDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [HomeRentDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [HomeRentDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [HomeRentDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [HomeRentDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [HomeRentDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [HomeRentDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [HomeRentDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [HomeRentDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [HomeRentDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [HomeRentDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [HomeRentDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [HomeRentDB] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [HomeRentDB] SET  MULTI_USER 
GO
ALTER DATABASE [HomeRentDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [HomeRentDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [HomeRentDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [HomeRentDB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [HomeRentDB]
GO
/****** Object:  Table [dbo].[Area]    Script Date: 6/23/2016 1:17:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Area](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[CityID] [int] NOT NULL,
 CONSTRAINT [PK_Area] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Bank]    Script Date: 6/23/2016 1:17:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Bank](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[BankName] [varchar](50) NULL,
	[TransitionSlip] [varchar](50) NULL,
	[Amount] [float] NULL,
	[Verfied] [bit] NULL,
 CONSTRAINT [PK_Bank] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BankAccount]    Script Date: 6/23/2016 1:17:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BankAccount](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RenterName] [varchar](50) NULL,
	[Renter NID] [int] NULL,
	[RenterProfession] [varchar](50) NULL,
	[HouseID] [int] NULL,
	[HouseRentID] [int] NULL,
	[Rent] [int] NULL,
	[Tax] [float] NULL,
	[OwnerName] [varchar](50) NULL,
	[RentSlipNo] [varchar](50) NULL,
	[BankName] [varchar](50) NULL,
	[AccountName] [varchar](50) NOT NULL,
	[AccountNumber] [varchar](50) NOT NULL,
	[FlatID] [int] NOT NULL,
	[UserID] [int] NOT NULL,
 CONSTRAINT [PK_BankAccount] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Category]    Script Date: 6/23/2016 1:17:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Category](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Lift] [bit] NOT NULL,
	[Security] [bit] NOT NULL,
	[Swimmingpool] [bit] NOT NULL,
	[Gim] [bit] NOT NULL,
	[FullTiles] [bit] NOT NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[City]    Script Date: 6/23/2016 1:17:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[City](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_City] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FixRent]    Script Date: 6/23/2016 1:17:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FixRent](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Rent] [int] NULL,
	[AreaID] [int] NULL,
	[CategoryID] [int] NULL,
 CONSTRAINT [PK_FixRent] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Flat]    Script Date: 6/23/2016 1:17:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Flat](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Size] [int] NOT NULL,
	[HouseID] [int] NOT NULL,
	[CategoryID] [int] NULL,
 CONSTRAINT [PK_Flat] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ForRent]    Script Date: 6/23/2016 1:17:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ForRent](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NOT NULL,
	[AvailableDate] [datetime] NOT NULL,
	[FlatID] [int] NOT NULL,
 CONSTRAINT [PK_ForRent] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[House]    Script Date: 6/23/2016 1:17:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[House](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[OwnerName] [varchar](50) NOT NULL,
	[NID] [varchar](50) NOT NULL,
	[PhoneNumber] [int] NOT NULL,
	[DateCreated] [varchar](50) NOT NULL,
	[HouseNo] [varchar](50) NULL,
	[RoadID] [int] NOT NULL,
	[UserID] [int] NULL,
 CONSTRAINT [PK_House] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HouseRent]    Script Date: 6/23/2016 1:17:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HouseRent](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Rent/PSFT] [int] NOT NULL,
	[FlatID] [int] NOT NULL,
	[AreaID] [int] NOT NULL,
	[CategoryID] [int] NOT NULL,
	[FlatSize] [int] NULL,
	[TotalRent] [int] NULL,
 CONSTRAINT [PK_HouseRent] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Payment]    Script Date: 6/23/2016 1:17:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Payment](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NOT NULL,
	[Amount] [varchar](50) NOT NULL,
	[FlatID] [int] NOT NULL,
	[UserID] [int] NOT NULL,
 CONSTRAINT [PK_Payment] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Rented]    Script Date: 6/23/2016 1:17:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Rented](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NOT NULL,
	[Rate] [varchar](50) NOT NULL,
	[Remarks] [varchar](50) NOT NULL,
	[Tax%] [varchar](50) NOT NULL,
	[FlatID] [int] NOT NULL,
	[UserID] [int] NOT NULL,
 CONSTRAINT [PK_Rented] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Road]    Script Date: 6/23/2016 1:17:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Road](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[AreaID] [int] NOT NULL,
 CONSTRAINT [PK_Road] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[User]    Script Date: 6/23/2016 1:17:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[User](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[NID] [varchar](50) NOT NULL,
	[PhoneNumber] [varchar](50) NOT NULL,
	[Profession] [varchar](50) NOT NULL,
	[Email] [varchar](max) NOT NULL,
	[Password] [nvarchar](max) NOT NULL,
	[Role] [varchar](50) NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Area] ON 

INSERT [dbo].[Area] ([ID], [Name], [CityID]) VALUES (1, N'Farmgate', 1)
INSERT [dbo].[Area] ([ID], [Name], [CityID]) VALUES (2, N'Dhanmondi', 1)
INSERT [dbo].[Area] ([ID], [Name], [CityID]) VALUES (3, N'Mohammadpur', 1)
SET IDENTITY_INSERT [dbo].[Area] OFF
SET IDENTITY_INSERT [dbo].[Category] ON 

INSERT [dbo].[Category] ([ID], [Name], [Lift], [Security], [Swimmingpool], [Gim], [FullTiles]) VALUES (1, N'Luxury', 1, 1, 1, 1, 1)
INSERT [dbo].[Category] ([ID], [Name], [Lift], [Security], [Swimmingpool], [Gim], [FullTiles]) VALUES (2, N'Executive', 1, 1, 0, 0, 1)
INSERT [dbo].[Category] ([ID], [Name], [Lift], [Security], [Swimmingpool], [Gim], [FullTiles]) VALUES (3, N'Economy', 1, 1, 0, 0, 0)
INSERT [dbo].[Category] ([ID], [Name], [Lift], [Security], [Swimmingpool], [Gim], [FullTiles]) VALUES (4, N'General', 0, 1, 0, 0, 0)
SET IDENTITY_INSERT [dbo].[Category] OFF
SET IDENTITY_INSERT [dbo].[City] ON 

INSERT [dbo].[City] ([ID], [Name]) VALUES (1, N'dhaka')
INSERT [dbo].[City] ([ID], [Name]) VALUES (2, N'khulna')
INSERT [dbo].[City] ([ID], [Name]) VALUES (1002, N'Comilla')
INSERT [dbo].[City] ([ID], [Name]) VALUES (1003, N'Rajsahi')
SET IDENTITY_INSERT [dbo].[City] OFF
SET IDENTITY_INSERT [dbo].[FixRent] ON 

INSERT [dbo].[FixRent] ([ID], [Rent], [AreaID], [CategoryID]) VALUES (1, 25, 1, 1)
INSERT [dbo].[FixRent] ([ID], [Rent], [AreaID], [CategoryID]) VALUES (2, 20, 1, 2)
INSERT [dbo].[FixRent] ([ID], [Rent], [AreaID], [CategoryID]) VALUES (3, 17, 1, 3)
INSERT [dbo].[FixRent] ([ID], [Rent], [AreaID], [CategoryID]) VALUES (4, 30, 2, 1)
INSERT [dbo].[FixRent] ([ID], [Rent], [AreaID], [CategoryID]) VALUES (5, 22, 2, 2)
INSERT [dbo].[FixRent] ([ID], [Rent], [AreaID], [CategoryID]) VALUES (6, 18, 2, 3)
INSERT [dbo].[FixRent] ([ID], [Rent], [AreaID], [CategoryID]) VALUES (7, 10, 2, 4)
INSERT [dbo].[FixRent] ([ID], [Rent], [AreaID], [CategoryID]) VALUES (8, 40, 3, 1)
INSERT [dbo].[FixRent] ([ID], [Rent], [AreaID], [CategoryID]) VALUES (9, 35, 3, 2)
SET IDENTITY_INSERT [dbo].[FixRent] OFF
SET IDENTITY_INSERT [dbo].[Flat] ON 

INSERT [dbo].[Flat] ([ID], [Name], [Size], [HouseID], [CategoryID]) VALUES (3, N'1-A', 1250, 6, 3)
INSERT [dbo].[Flat] ([ID], [Name], [Size], [HouseID], [CategoryID]) VALUES (4, N'2-q', 1250, 7, 1)
INSERT [dbo].[Flat] ([ID], [Name], [Size], [HouseID], [CategoryID]) VALUES (1004, N'Sotodol', 1250, 1, 1)
INSERT [dbo].[Flat] ([ID], [Name], [Size], [HouseID], [CategoryID]) VALUES (1005, N'2-A', 1150, 1, 2)
INSERT [dbo].[Flat] ([ID], [Name], [Size], [HouseID], [CategoryID]) VALUES (1006, N'3-A', 1000, 1, 3)
INSERT [dbo].[Flat] ([ID], [Name], [Size], [HouseID], [CategoryID]) VALUES (1007, N'3-B', 1000, 1, 4)
INSERT [dbo].[Flat] ([ID], [Name], [Size], [HouseID], [CategoryID]) VALUES (1008, N'1-C', 1150, 1, 3)
INSERT [dbo].[Flat] ([ID], [Name], [Size], [HouseID], [CategoryID]) VALUES (1009, N'2-D', 1450, 1, 2)
INSERT [dbo].[Flat] ([ID], [Name], [Size], [HouseID], [CategoryID]) VALUES (1010, N'5-D', 1250, 7, 1)
INSERT [dbo].[Flat] ([ID], [Name], [Size], [HouseID], [CategoryID]) VALUES (1011, N'3-C', 1050, 1, 2)
INSERT [dbo].[Flat] ([ID], [Name], [Size], [HouseID], [CategoryID]) VALUES (1012, N'4-c', 1203, 6, 2)
INSERT [dbo].[Flat] ([ID], [Name], [Size], [HouseID], [CategoryID]) VALUES (1013, N'1-A', 1150, 5, 1)
INSERT [dbo].[Flat] ([ID], [Name], [Size], [HouseID], [CategoryID]) VALUES (1014, N'2-C', 1200, 3, 1)
INSERT [dbo].[Flat] ([ID], [Name], [Size], [HouseID], [CategoryID]) VALUES (1015, N'4-A', 950, 1, 1)
SET IDENTITY_INSERT [dbo].[Flat] OFF
SET IDENTITY_INSERT [dbo].[House] ON 

INSERT [dbo].[House] ([ID], [Name], [OwnerName], [NID], [PhoneNumber], [DateCreated], [HouseNo], [RoadID], [UserID]) VALUES (1, N'kobutor monjil', N'machine', N'0122222222225535454', 1675011236, N'12-15-2016', NULL, 8, 7)
INSERT [dbo].[House] ([ID], [Name], [OwnerName], [NID], [PhoneNumber], [DateCreated], [HouseNo], [RoadID], [UserID]) VALUES (2, N'Ma Vila', N'Sajjad', N'145200000000', 1674441234, N'12-14-15', NULL, 1, 4)
INSERT [dbo].[House] ([ID], [Name], [OwnerName], [NID], [PhoneNumber], [DateCreated], [HouseNo], [RoadID], [UserID]) VALUES (3, N'Sotodol', N'Mahbub', N'10000000000000000', 1671111321, N'12-12-12', NULL, 10, 0)
INSERT [dbo].[House] ([ID], [Name], [OwnerName], [NID], [PhoneNumber], [DateCreated], [HouseNo], [RoadID], [UserID]) VALUES (4, N'Jahan Vila', N'Shajahan', N'124562012345', 1658952125, N'11-12-15', NULL, 3, 0)
INSERT [dbo].[House] ([ID], [Name], [OwnerName], [NID], [PhoneNumber], [DateCreated], [HouseNo], [RoadID], [UserID]) VALUES (5, N'Nikunjo', N'Afia Begum', N'145236555555', 1671234567, N'12-12-12', N'2-b', 9, 0)
INSERT [dbo].[House] ([ID], [Name], [OwnerName], [NID], [PhoneNumber], [DateCreated], [HouseNo], [RoadID], [UserID]) VALUES (6, N'anando garden', N'Mr.x', N'11111111111111111', 1854652145, N'12-12-12', N'4-b', 10, 1007)
INSERT [dbo].[House] ([ID], [Name], [OwnerName], [NID], [PhoneNumber], [DateCreated], [HouseNo], [RoadID], [UserID]) VALUES (7, N'Sotodol', N'Mahbub', N'10000000000000000', 1671111321, N'12-12-12', N'1-A', 10, 1007)
SET IDENTITY_INSERT [dbo].[House] OFF
SET IDENTITY_INSERT [dbo].[HouseRent] ON 

INSERT [dbo].[HouseRent] ([ID], [Rent/PSFT], [FlatID], [AreaID], [CategoryID], [FlatSize], [TotalRent]) VALUES (1016, 30, 1014, 2, 1, 1200, 36000)
INSERT [dbo].[HouseRent] ([ID], [Rent/PSFT], [FlatID], [AreaID], [CategoryID], [FlatSize], [TotalRent]) VALUES (1017, 35, 1013, 3, 2, 1150, 40250)
INSERT [dbo].[HouseRent] ([ID], [Rent/PSFT], [FlatID], [AreaID], [CategoryID], [FlatSize], [TotalRent]) VALUES (1018, 25, 1015, 1, 1, 950, 23750)
INSERT [dbo].[HouseRent] ([ID], [Rent/PSFT], [FlatID], [AreaID], [CategoryID], [FlatSize], [TotalRent]) VALUES (1019, 20, 1015, 1, 2, 950, 19000)
INSERT [dbo].[HouseRent] ([ID], [Rent/PSFT], [FlatID], [AreaID], [CategoryID], [FlatSize], [TotalRent]) VALUES (1020, 35, 1009, 3, 2, 1450, 50750)
INSERT [dbo].[HouseRent] ([ID], [Rent/PSFT], [FlatID], [AreaID], [CategoryID], [FlatSize], [TotalRent]) VALUES (1021, 40, 3, 3, 1, 1250, 50000)
INSERT [dbo].[HouseRent] ([ID], [Rent/PSFT], [FlatID], [AreaID], [CategoryID], [FlatSize], [TotalRent]) VALUES (1024, 35, 1012, 3, 2, 1203, 42105)
INSERT [dbo].[HouseRent] ([ID], [Rent/PSFT], [FlatID], [AreaID], [CategoryID], [FlatSize], [TotalRent]) VALUES (2012, 20, 4, 1, 2, 1250, 25000)
INSERT [dbo].[HouseRent] ([ID], [Rent/PSFT], [FlatID], [AreaID], [CategoryID], [FlatSize], [TotalRent]) VALUES (2013, 17, 1010, 1, 3, 1250, 21250)
SET IDENTITY_INSERT [dbo].[HouseRent] OFF
SET IDENTITY_INSERT [dbo].[Road] ON 

INSERT [dbo].[Road] ([ID], [Name], [AreaID]) VALUES (1, N'road-2', 2)
INSERT [dbo].[Road] ([ID], [Name], [AreaID]) VALUES (2, N'road-3', 2)
INSERT [dbo].[Road] ([ID], [Name], [AreaID]) VALUES (3, N'road-4', 2)
INSERT [dbo].[Road] ([ID], [Name], [AreaID]) VALUES (4, N'Mod', 3)
INSERT [dbo].[Road] ([ID], [Name], [AreaID]) VALUES (5, N'Mod-1', 3)
INSERT [dbo].[Road] ([ID], [Name], [AreaID]) VALUES (6, N'Mod-2', 3)
INSERT [dbo].[Road] ([ID], [Name], [AreaID]) VALUES (8, N'Farm-1', 1)
INSERT [dbo].[Road] ([ID], [Name], [AreaID]) VALUES (9, N'Farm-2', 1)
INSERT [dbo].[Road] ([ID], [Name], [AreaID]) VALUES (10, N'Nurjahan road', 3)
SET IDENTITY_INSERT [dbo].[Road] OFF
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([ID], [Name], [NID], [PhoneNumber], [Profession], [Email], [Password], [Role]) VALUES (1, N'Jewel', N'121212423263123', N'01908675432', N'software eng', N'ag@gmail.com', N'123456', N'House owner')
INSERT [dbo].[User] ([ID], [Name], [NID], [PhoneNumber], [Profession], [Email], [Password], [Role]) VALUES (2, N'Jewel', N'0122222222225535454', N'24354435', N'software eng', N'je026i@gmail.com', N'lkdvffbggfbd', N'House owner')
INSERT [dbo].[User] ([ID], [Name], [NID], [PhoneNumber], [Profession], [Email], [Password], [Role]) VALUES (3, N'disha', N'0122222222225535454', N'01908675432', N'house wife', N'shohely@gmail.com', N'disha123', N'rentee')
INSERT [dbo].[User] ([ID], [Name], [NID], [PhoneNumber], [Profession], [Email], [Password], [Role]) VALUES (4, N'shibli', N'0122222222225535454', N'01908675432', N'house wife', N's@gmail.com', N'asdfg', N'House Owner')
INSERT [dbo].[User] ([ID], [Name], [NID], [PhoneNumber], [Profession], [Email], [Password], [Role]) VALUES (5, N'shibli', N'0122222222225535454', N'01908675432', N'house wife', N's@gmail.com', N'asdfg', N'House Owner')
INSERT [dbo].[User] ([ID], [Name], [NID], [PhoneNumber], [Profession], [Email], [Password], [Role]) VALUES (6, N'sumon', N'1444444444444444444', N'01675011236', N'teacher', N's@gmail.com', N'123456', N'House Owner')
INSERT [dbo].[User] ([ID], [Name], [NID], [PhoneNumber], [Profession], [Email], [Password], [Role]) VALUES (7, N'haque', N'10000000000000000000000', N'01675011236', N'doctor', N'shohely.haque@gmail.com', N'disha0000', N'House Owner')
INSERT [dbo].[User] ([ID], [Name], [NID], [PhoneNumber], [Profession], [Email], [Password], [Role]) VALUES (1007, N'omi', N'10456666666666', N'016745678912', N'business', N'j@gmail.com', N'000000', N'House Owner')
INSERT [dbo].[User] ([ID], [Name], [NID], [PhoneNumber], [Profession], [Email], [Password], [Role]) VALUES (1008, N'disha', N'122222255555', N'016750235', N'business', N'd@gmail.com', N'disha', N'House Owner')
INSERT [dbo].[User] ([ID], [Name], [NID], [PhoneNumber], [Profession], [Email], [Password], [Role]) VALUES (1009, N'Avisek', N'19201223654789', N'01658952125', N'business', N's@gmail.com', N'000000', N'House Owner')
SET IDENTITY_INSERT [dbo].[User] OFF
ALTER TABLE [dbo].[Area]  WITH CHECK ADD  CONSTRAINT [FK_Area_City] FOREIGN KEY([CityID])
REFERENCES [dbo].[City] ([ID])
GO
ALTER TABLE [dbo].[Area] CHECK CONSTRAINT [FK_Area_City]
GO
ALTER TABLE [dbo].[BankAccount]  WITH CHECK ADD  CONSTRAINT [FK_BankAccount_Flat] FOREIGN KEY([ID])
REFERENCES [dbo].[House] ([ID])
GO
ALTER TABLE [dbo].[BankAccount] CHECK CONSTRAINT [FK_BankAccount_Flat]
GO
ALTER TABLE [dbo].[BankAccount]  WITH CHECK ADD  CONSTRAINT [FK_BankAccount_House] FOREIGN KEY([HouseID])
REFERENCES [dbo].[House] ([ID])
GO
ALTER TABLE [dbo].[BankAccount] CHECK CONSTRAINT [FK_BankAccount_House]
GO
ALTER TABLE [dbo].[BankAccount]  WITH CHECK ADD  CONSTRAINT [FK_BankAccount_HouseRent] FOREIGN KEY([HouseRentID])
REFERENCES [dbo].[HouseRent] ([ID])
GO
ALTER TABLE [dbo].[BankAccount] CHECK CONSTRAINT [FK_BankAccount_HouseRent]
GO
ALTER TABLE [dbo].[BankAccount]  WITH CHECK ADD  CONSTRAINT [FK_BankAccount_User] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[BankAccount] CHECK CONSTRAINT [FK_BankAccount_User]
GO
ALTER TABLE [dbo].[FixRent]  WITH CHECK ADD  CONSTRAINT [FK_FixRent_Area] FOREIGN KEY([AreaID])
REFERENCES [dbo].[Area] ([ID])
GO
ALTER TABLE [dbo].[FixRent] CHECK CONSTRAINT [FK_FixRent_Area]
GO
ALTER TABLE [dbo].[FixRent]  WITH CHECK ADD  CONSTRAINT [FK_FixRent_Category] FOREIGN KEY([CategoryID])
REFERENCES [dbo].[Category] ([ID])
GO
ALTER TABLE [dbo].[FixRent] CHECK CONSTRAINT [FK_FixRent_Category]
GO
ALTER TABLE [dbo].[Flat]  WITH CHECK ADD  CONSTRAINT [FK_Flat_Category] FOREIGN KEY([CategoryID])
REFERENCES [dbo].[Category] ([ID])
GO
ALTER TABLE [dbo].[Flat] CHECK CONSTRAINT [FK_Flat_Category]
GO
ALTER TABLE [dbo].[Flat]  WITH CHECK ADD  CONSTRAINT [FK_Flat_House] FOREIGN KEY([HouseID])
REFERENCES [dbo].[House] ([ID])
GO
ALTER TABLE [dbo].[Flat] CHECK CONSTRAINT [FK_Flat_House]
GO
ALTER TABLE [dbo].[ForRent]  WITH CHECK ADD  CONSTRAINT [FK_ForRent_Flat] FOREIGN KEY([FlatID])
REFERENCES [dbo].[Flat] ([ID])
GO
ALTER TABLE [dbo].[ForRent] CHECK CONSTRAINT [FK_ForRent_Flat]
GO
ALTER TABLE [dbo].[HouseRent]  WITH CHECK ADD  CONSTRAINT [FK_HouseRent_Area] FOREIGN KEY([AreaID])
REFERENCES [dbo].[Area] ([ID])
GO
ALTER TABLE [dbo].[HouseRent] CHECK CONSTRAINT [FK_HouseRent_Area]
GO
ALTER TABLE [dbo].[HouseRent]  WITH CHECK ADD  CONSTRAINT [FK_HouseRent_Category] FOREIGN KEY([CategoryID])
REFERENCES [dbo].[Category] ([ID])
GO
ALTER TABLE [dbo].[HouseRent] CHECK CONSTRAINT [FK_HouseRent_Category]
GO
ALTER TABLE [dbo].[HouseRent]  WITH CHECK ADD  CONSTRAINT [FK_HouseRent_Flat] FOREIGN KEY([FlatID])
REFERENCES [dbo].[Flat] ([ID])
GO
ALTER TABLE [dbo].[HouseRent] CHECK CONSTRAINT [FK_HouseRent_Flat]
GO
ALTER TABLE [dbo].[Payment]  WITH CHECK ADD  CONSTRAINT [FK_Payment_Flat] FOREIGN KEY([FlatID])
REFERENCES [dbo].[Flat] ([ID])
GO
ALTER TABLE [dbo].[Payment] CHECK CONSTRAINT [FK_Payment_Flat]
GO
ALTER TABLE [dbo].[Payment]  WITH CHECK ADD  CONSTRAINT [FK_Payment_User] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Payment] CHECK CONSTRAINT [FK_Payment_User]
GO
ALTER TABLE [dbo].[Rented]  WITH CHECK ADD  CONSTRAINT [FK_Rented_Flat] FOREIGN KEY([FlatID])
REFERENCES [dbo].[Flat] ([ID])
GO
ALTER TABLE [dbo].[Rented] CHECK CONSTRAINT [FK_Rented_Flat]
GO
ALTER TABLE [dbo].[Rented]  WITH CHECK ADD  CONSTRAINT [FK_Rented_User] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Rented] CHECK CONSTRAINT [FK_Rented_User]
GO
ALTER TABLE [dbo].[Road]  WITH CHECK ADD  CONSTRAINT [FK_Road_Area] FOREIGN KEY([AreaID])
REFERENCES [dbo].[Area] ([ID])
GO
ALTER TABLE [dbo].[Road] CHECK CONSTRAINT [FK_Road_Area]
GO
USE [master]
GO
ALTER DATABASE [HomeRentDB] SET  READ_WRITE 
GO
