﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HomeRent.Models;

namespace HomeRent.Controllers
{
    public class AdminController : Controller
    {

        private HomeRentDB db = new HomeRentDB();
        //
        // GET: /Admin/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Login()
        {

            return View();
        }

        public ActionResult LogOut()
        {
            if (ModelState.IsValid)
            {
                Session.Clear();
            }
            return View();
        }
        [HttpPost]

        public ActionResult Login(Admin admin)
        {
            if (ModelState.IsValid)
            {
                var v = db.Admins.Where(a => a.Name.ToLower() == admin.Name.ToLower() & a.Passwor == admin.Passwor).ToList();
              


                    if (v.Count > 0)
                    {
                        Session["ID"] = v.First().ID;

                        Session["Name"] = v.First().Name;
                        
                        Session["Password"] = v.First().Passwor;
                       
                        return RedirectToAction("Index");

                    }
                    
                }
                
            
            return View();
        }
    }
}



        
  